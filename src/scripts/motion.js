/**
 * motion related functions
 */

function computerPlayerMovement() {
    let paddleCenter = yPaddle2 + paddleHeight / 2;
    if (paddleCenter < yBall - 35) {
        yPaddle2 += 6;
    } else if (paddleCenter > yBall + 35) {
        yPaddle2 -= 6;
    }
}

function movements() {
    if (showWinScreen) {
        return;
    }

    computerPlayerMovement();

    // update ball
    xBall += xBallSpeed;
    yBall += yBallSpeed;

    // update left side
    if (xBall < 0) {
        if (yBall > yPaddle1 && yBall < yPaddle1 + paddleHeight) {
            xBallSpeed = -xBallSpeed;
            let yDelta = yBall - (yPaddle1 + paddleHeight / 2);
            yBallSpeed = yDelta * 0.15;
        } else {
            computerScore++;
            ballReset();
        }
    }

    // update right side
    if (xBall > canvas.width) {
        if (yBall > yPaddle2 && yBall < yPaddle2 + paddleHeight) {
            xBallSpeed = -xBallSpeed;

            let yDelta = yBall - (yPaddle2 + paddleHeight / 2);
            yBallSpeed = yDelta * 0.15;
        } else {
            playerScore++;
            ballReset();
        }
    }

    yBall += yBallSpeed;
    if (yBall > canvas.height || yBall < 0) {
        yBallSpeed = -yBallSpeed;
    }
}
