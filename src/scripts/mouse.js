/**
 * handlers for the mouse activity
 */

function calcMousePosition(e) {
    let rect = canvas.getBoundingClientRect();
    let root = document.documentElement;
    let xMouse = e.clientX - rect.left - root.scrollLeft;
    let yMouse = e.clientY - rect.top - root.scrollTop;

    return {
        x: xMouse,
        y: yMouse,
    };
}

function handleMouseClick() {
    if (showWinScreen) {
        playerScore = 0;
        computerScore = 0;
        showWinScreen = false;
    }
}
