/** @type {HTMLCanvasElement} */
const canvas = document.getElementById('gameCanvas');
const ctx = canvas.getContext('2d');

canvas.width = 1000;
canvas.height = 600;

const fps = 60;
let xBall = 50;
let yBall = 50;
let xBallSpeed = 7;
let yBallSpeed = 2;

let yPaddle1;
let yPaddle2 = canvas.height / 2;

const paddleHeight = 100;
const paddleThickness = 8;
const paddleBuffer = 2;

let showWinScreen = false;
let playerScore = 0;
let computerScore = 0;
const winningScore = 5;

function ballReset() {
    if (playerScore >= winningScore || computerScore >= winningScore) {
        showWinScreen = true;
    }

    xBall = canvas.width / 2;
    yBall = canvas.height / 2;
    xBallSpeed = -xBallSpeed;
    yBallSpeed = 1;
}

// run the gameloop
setInterval(() => {
    movements();
    draw();
}, 1000 / fps);

canvas.addEventListener('mousemove', function (e) {
    let mousePos = calcMousePosition(e);
    yPaddle1 = mousePos.y - paddleHeight / 2;
});

canvas.addEventListener('mousedown', handleMouseClick);
