/**
 * drawing related functions and wrappers
 */

function colorRect(xLeft, yTop, width, height, drawColor) {
    ctx.fillStyle = drawColor;
    ctx.fillRect(xLeft, yTop, width, height);
}

function colorCircle(xCentre, yCentre, radius, drawColor) {
    ctx.fillStyle = drawColor;
    ctx.beginPath();
    ctx.arc(xCentre, yCentre, radius, 0, Math.PI * 2, true);
    ctx.fill();
}

function drawNet() {
    for (i = 20; i < canvas.height - 20; i += 45) {
        colorRect(canvas.width / 2 - 1, i, 2, 20, 'ivory');
    }
}

function draw() {
    // draw play field
    colorRect(0, 0, canvas.width, canvas.height, '#111');

    if (showWinScreen) {
        ctx.fillStyle = 'ivory';
        ctx.fillText('click to continue', canvas.width / 2, canvas.height / 2);
        //return;
    }

    drawNet();

    // paddle
    colorRect(paddleBuffer, yPaddle1, paddleThickness, paddleHeight, 'oldlace');

    // paddle 2 (computer player)
    colorRect(
        canvas.width - (paddleThickness + paddleBuffer),
        yPaddle2,
        paddleThickness,
        paddleHeight,
        'oldlace'
    );

    // draw the ball
    colorCircle(xBall, yBall, 10, 'orangered');

    // scores
    ctx.font = '15px fantasy';
    ctx.fillStyle = 'ivory';
    ctx.fillText('Player 1', 100, 100);
    ctx.font = '18px fantasy';
    ctx.textAlign = 'center';
    ctx.fillText(playerScore, 100, 125);

    ctx.font = '15px fantasy';
    ctx.fillStyle = 'ivory';
    ctx.fillText('Computer', canvas.width - 100, 100);
    ctx.font = '18px fantasy';
    ctx.textAlign = 'center';
    ctx.fillText(computerScore, canvas.width - 100, 125);
}
